public class AStack<T>
{
   private T[] arr;
   private int top;
   
   public AStack(int size)
   {
      arr = (T[])new Object[size];
      top = -1;
   }
   
   public static class MyException extends RuntimeException
   {
      public MyException()
      {
         super();
      }
      
      public MyException(String msg)
      {
         super(msg);
      }
   }
   
   public void push(T data)
   {
      int i;
      if(top == arr.length - 1)
      {
         T[] bigArr = (T[])new Object[top * 2];
         
         for(i = 0; i < arr.length; i++)
         {
            bigArr[i] = arr[i];
         }
         top++;
         bigArr[top] = data;
         arr = bigArr;
      }
      else
      {
         top++;
         arr[top] = data;
      }
   }
   
   public T pop()
   {
      T temp;
      
      if(top == -1)
      {
         throw new MyException();
      }
      else
      {
         temp = arr[top];
         top--;
      }
      return temp;
   }
   
   public T peek()
   {
      T temp;
      if(top == -1)
      {
         throw new MyException();
      }
      else
      {
         temp = arr[top];
      }
      return temp;
   }
   
   public boolean isEmpty()
   {
      if(top == -1)
      {
         return true;
      }
      else
      {
         return false;
      }
   }
   
}
        