import java.util.*;

public class StringChecker {
   
   public static boolean isBalanced(String message){
      
      AStack<Character> stack = new AStack<Character>(message.length());
      boolean balanced = true;
      Character temp;
      
      for(int i=0; i<message.length(); i++){
         if(message.charAt(i) == '(' || message.charAt(i) == '{' || message.charAt(i) == '['){
            stack.push(message.charAt(i));
         }
         else if(message.charAt(i) == ')'){
            if(stack.isEmpty()){
               return false;
            }
            else{
               if(stack.pop() != '('){
                  return false;   
               }   
            }
         }
         else if (message.charAt(i) == '}'){
            if(stack.isEmpty()){
               return false;
            }
            else{
               if(stack.pop() != '{')
                  return false;
            }
         }
         else if (message.charAt(i) == ']'){
            
            if(stack.isEmpty()){
               return false;
            }
            else{
               if(stack.pop() != '[')
                  return false;
            }
            
         }
    
      }
      
      if(!stack.isEmpty())
         return false;
      
      return true;
      
   }

   public static void main(String[] args){
      
      Scanner input = new Scanner(System.in);
      String decision = "y";
      String temp;

            
      while(decision.equals("y")){
         System.out.print("\nPlease enter string to be checked: ");
         temp = input.nextLine();
         if(StringChecker.isBalanced(temp))
            System.out.println("String is balanced");
         else
            System.out.println("String is NOT balanced");
                  
  
         System.out.print("Would you like to continue (y/n): ");
         decision = input.nextLine();
      }

   }
}
